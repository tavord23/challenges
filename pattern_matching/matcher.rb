class Matcher
  attr_accessor :pattern, :string

  def initialize(pattern, string)
    @pattern = pattern
    @string = string
  end

  def matches?
    return true if non_repeating_pattern?

    pattern_details.values.each do |data|
      return false if difference_at_positions(data[:positions])
    end
    true
  end

  private

  def difference_at_positions(positions)
    string_parts.values_at(*positions).uniq.count > 1
  end

  def non_repeating_pattern?
    pattern.split("").uniq.count == pattern.length
  end

  def pattern_details
    position_in_pattern = 0
    @pattern_details ||= pattern.each_char.with_object({}) do |char, hsh|
      hsh[char] = { positions: [], count: 0 } if hsh[char].nil?
      hsh[char][:count] = hsh[char][:count] + 1
      hsh[char][:positions] << position_in_pattern
      position_in_pattern += 1
    end
  end

  def string_parts
    split_string = [string]
    pattern_count = 0
    pattern_details.each do |_key, value|
      loop do
        pattern_count += value[:count]
        split_string = split_string.map do |word|
          slice_in = (word.length.to_f / value[:count].to_f).ceil
          word.chars.each_slice(slice_in).map(&:join)
        end.flatten
        break if split_string.count >= pattern_count
      end
    end
    split_string
  end
end
