require "./matcher"

describe Matcher do
  it "is the same pattern" do
    expect(Matcher.new("abab", "redblueredblue").matches?).to be true
    expect(Matcher.new("aaaa", "asdasdasdasd").matches?).to be true
    expect(Matcher.new("aabb", "xyzabcxzyabc").matches?).to be false
    expect(Matcher.new("bbaa", "xyzxyzabcabc").matches?).to be true
    expect(Matcher.new("abc", "foobarbaz").matches?).to be true
  end
end
