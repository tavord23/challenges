Given a pattern and a string input - find if the string follows the same pattern and return true or false.

Examples:

Pattern : "abab", input: "redblueredblue" should return true.
Pattern: "aaaa", input: "asdasdasdasd" should return true.
Pattern: "aabb", input: "xyzabcxzyabc" should return false.

Please code your solution using ruby.

## Testing

In order to test the class, just run `bundle exec rspec matcher_spec.rb`

## Conclusion

After some time trying to find a way to map a letter in the pattern with a potential word in the string. I thought I don't really care what words we have, all I care is that a pattern matches.

So the approach I took was to split the string into the different patterns found keeping an eye on the position of the pattern.

After that, all I need to do is check if the positions at some stored indexes are equal between them, if so, it means the pattern is being followed.

Being completely honest, this has been a nice challenge and I think this can be improved to use less loops, after being struggling for a while I went to Google and found out this seems to be a common problem that can be solved with a Backtracking algorithm which was already there waiting to be copied, and I kind of felt like cheating, so I decided to keep this for the sake of the exercise.

I know this solution is not 100% failproof, I'm aware there are some scenarios that might fail.

It's already been a couple of days, and wanted to submit something, if you agree, I could take some time over the weekend to try to find alternate solutions that don't feel like cheating.
